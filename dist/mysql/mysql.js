"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mysql = require("mysql");
class Mysql {
    constructor() {
        this.conectado = false;
        console.log('Clase inicializada');
        this.cnn = mysql.createConnection({
            host: 'localhost',
            user: 'node_user',
            password: '123456',
            database: 'node_db'
        });
        this.conectarDB();
    }
    static get instance() {
        return this._instance || (this._instance = new this());
    }
    static ejecutarQuery(query, callback) {
        this.instance.cnn.query(query, (err, results, fields) => {
            if (err) {
                console.log(err);
                callback(err);
            }
            if (results.length === 0) {
                callback('Datos no encontrados');
            }
            callback(null, results);
        });
        // console.log("finish");
        // this.instance.cnn.end();
    }
    conectarDB() {
        this.cnn.connect((err) => {
            if (err) {
                console.log(err);
                return;
            }
            this.conectado = true;
            console.log('BD activa');
        });
    }
}
exports.default = Mysql;
