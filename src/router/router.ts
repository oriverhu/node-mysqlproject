import {Router, Request, Response} from 'express';
import Mysql from '../mysql/mysql';

const router = Router();
router.get('/heroes', (req: Request, res: Response)=>{

    const query = `SELECT * FROM heroes`;
    Mysql.ejecutarQuery(query, (err: any, data: Object[])=>{
        if(err){
            res.status(400).json({
                ok: false,
                error: err
            })
        }

        res.json({
            ok:true,
            heroes: data
        })
    })

})
router.get('/heroe/:id', (req: Request, res: Response)=>{
    const id = req.params.id;
    const query = `SELECT * FROM heroes where id=${id}`;
    Mysql.ejecutarQuery(query, (err: any, data: Object[])=>{
        if(err){
            res.status(400).json({
                ok: false,
                error: err
            })
        }

        res.json({
            ok:true,
            heroe: data[0]
        })
    })
})

export default router;